export interface Library {
  id: number;
  title: string;
  description: string;
  url: string;
  img: string;
  active: boolean;
}
