# Automated Testing

Fol-stack application for component-testing

<p>
<img src="https://gitlab.com/pascal.cantaluppi/testautomation/-/raw/main/apps/frontend/public/img/logo.png" width="100" alt="Logo" /></a>
</p>

## Description

This Project ist part of the automatest testing guidelines (CAS IT-PM FHNW).

It contains testing procedures for javascript full-stack development.

<p>
<img src="https://gitlab.com/pascal.cantaluppi/testautomation/-/raw/main/apps/frontend/public/img/frameworks.png" width="300" alt="Pipeline" /></a>
</p>

## Installation

This project uses [Node.js](https://nodejs.org/) as the JavaScript runtime.

```
git clone https://gitlab.com/pascal.cantaluppi/testautomation.git
cd testautomation
npm i
nx run-many --parallel --target=serve --projects=frontend,backend 
```

## Testing

Run automated tests
```
nx test backend
nx test frontend
```

Update Snapshots
```
jest --updateSnapshot
```

Coverage
```
jest --collectCoverage
```

## Deployment

This project is optimized for container virtualization.

<p>
<img src="https://gitlab.com/pascal.cantaluppi/testautomation/-/raw/main/apps/frontend/public/img/container.png" width="400" alt="Container" /></a>
</p>


The pipeline is runnig the test suite as defined in the ci configuration file.

<p>
<img src="https://gitlab.com/pascal.cantaluppi/testautomation/-/raw/main/apps/frontend/public/img/pipeline.png" width="400" alt="Pipeline" /></a>
</p>


## Preview

Backend:
<br />
[https://backend.cantaluppi.org/api/](https://backend.cantaluppi.org/api/)

Frontend:
<br />
[https://frontend.cantaluppi.org/](https://frontend.cantaluppi.org/)
