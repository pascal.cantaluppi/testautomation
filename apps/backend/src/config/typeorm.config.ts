import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Library } from '../library/library.entity';

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: process.env.NX_DB_HOST,
  port: parseInt(process.env.NX_DB_PORT),
  username: process.env.NX_DB_USERNAME,
  password: process.env.NX_DB_PASSWORD,
  database: process.env.NX_DB_NAME,
  synchronize: Boolean(process.env.NX_DB_SYNC),
  entities: [Library],
};
