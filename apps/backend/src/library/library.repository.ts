import { Repository, EntityRepository } from 'typeorm';
import { Library } from './library.entity';
import { CreateLibraryDto } from './dto/library.dto';
import { Logger, InternalServerErrorException } from '@nestjs/common';

@EntityRepository(Library)
export class LibraryRepository extends Repository<Library> {
  private logger = new Logger('LibraryRepository');

  async getLibraries(): Promise<Library[]> {
    const query = this.createQueryBuilder('library');
    try {
      const libraries = query.getMany();
      return libraries;
    } catch (error) {
      this.logger.error(`Failed to get libraries`, error.stack);
      throw new InternalServerErrorException('Failed to get libraries');
    }
  }

  async createLibrary(createLibraryDto: CreateLibraryDto): Promise<Library> {
    const { title, description, url, img, active } = createLibraryDto;
    const library = new Library();
    library.title = title;
    library.description = description;
    library.url = url;
    library.img = img;
    library.active = active;
    try {
      await library.save();
      return library;
    } catch (error) {
      this.logger.error(
        `Failed to create library. Data: ${JSON.stringify(createLibraryDto)}`,
        error.stack,
      );
      throw new InternalServerErrorException('Failed to get library');
    }
  }
}
