import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { CreateLibraryDto } from './dto/library.dto';
import { LibraryRepository } from './library.repository';
import { LibraryService } from './library.service';

const mockLibraryRepository = () => ({
  getLibraries: jest.fn(),
  findOne: jest.fn(),
  createLibrary: jest.fn(),
  delete: jest.fn(),
});

const mockLibrary = {
  id: 1,
  title: 'Jest',
  description:
    'Jest is a delightful JavaScript Testing Framework with focus on simplicity.',
  url: 'https://jestjs.io/',
  img: '/testing/jest.png',
  active: true,
};

const mockLibraries = [];
mockLibraries.push(mockLibrary);

describe('Library', () => {
  let libraryService: LibraryService;
  let libraryRepository;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        LibraryService,
        { provide: LibraryRepository, useFactory: mockLibraryRepository },
      ],
    }).compile();

    libraryService = module.get(LibraryService);
    libraryRepository = module.get(LibraryRepository);
  });

  describe('getLibraries', () => {
    it('calls getLibraries and mocks the array', async () => {
      console.log(mockLibraries);
      libraryRepository.getLibraries.mockResolvedValue(mockLibraries);
      const result = await libraryService.getLibraries();
      console.log(result);
      expect(result).toContain(mockLibrary);
    });
  });

  // describe('getLibraryById', () => {
  //   it('calls getLibraryById and mocks the object', async () => {
  //     libraryRepository.findOne.mockResolvedValue(mockLibrary);
  //     const result = await libraryService.getLibraryById(1);
  //     expect(result).toEqual(mockLibrary);
  //   });

  // it('calls getLibraryById and handles an error', async () => {
  //   expect(libraryService.getLibraryById(-1)).rejects.toThrow(
  //     NotFoundException,
  //   );
  // });

  // });

  // describe('createLibrary', () => {
  //   it('calls createLibrary and mocks the object', async () => {
  //     libraryRepository.createLibrary.mockResolvedValue(mockLibrary);
  //     const createTaskDto = {
  //       title: 'Jest',
  //       description:
  //         'Jest is a delightful JavaScript Testing Framework with focus on simplicity.',
  //       url: 'https://jestjs.io/',
  //       img: '/testing/jest.png',
  //       active: true,
  //     };
  //     const result = await libraryService.createLibrary(createTaskDto);
  //     expect(libraryRepository.createLibrary).toHaveBeenCalledWith(
  //       createTaskDto,
  //     );
  //     expect(result).toEqual(mockLibrary);
  //   });

  //   it('calls createLibrary and handles an error', async () => {
  //     expect(libraryService.createLibrary(null)).rejects.toThrow(
  //       BadRequestException,
  //     );
  //   });
  // });

  // const id = 0

  // const id = 14;

  // describe('test deleteLibrary', () => {
  //   let result: any;
  //   beforeEach(async () => {
  //     result = await libraryService.createLibrary(mockLibrary);
  //   });
  //   it('calls deleteLibrary with success', async () => {
  //     await libraryService.deleteLibrary(result.id);
  //     expect(libraryRepository.delete).toHaveBeenCalledWith({ id: id });
  //   });

  // it('calls deleteLibrary and handles an error', async () => {
  //   libraryRepository.delete.mockResolvedValue({ affected: 0 });
  //   expect(libraryService.deleteLibrary(1)).rejects.toThrow(
  //     NotFoundException,
  //   );
  // });
  // });

  // describe('deleteLibrary', () => {
  //   it('calls deleteLibrary with success', async () => {
  //     libraryRepository.delete.mockResolvedValue({ affected: 1 });
  //     expect(libraryRepository.delete).not.toHaveBeenCalled();
  //     await libraryService.deleteLibrary(1);
  //     expect(libraryRepository.delete).toHaveBeenCalledWith({ id: 1 });
  //   });
  //});

  describe('deleteLibrary', () => {
    it('calls deleteLibrary with success', async () => {
      await libraryService.deleteLibrary(1);
      expect(libraryRepository.delete).toHaveBeenCalledWith({ id: 1 });
    });
  });

  // expect(libraryService.deleteLibrary(-1)).rejects.toThrow(
  //   NotFoundException,
  // );

  // describe('updateLibraryTitle', () => {
  //   it('calls updateLibraryTitle  with success', async () => {
  //     const save = jest.fn().mockResolvedValue(true);

  //     libraryService.getTaskById = jest.fn().mockResolvedValue({
  //       status: TaskStatus.OPEN,
  //       save,
  //     });

  //     expect(libraryService.getTaskById).not.toHaveBeenCalled();
  //     expect(save).not.toHaveBeenCalled();
  //     const result = await libraryService.updateTaskStatus(
  //       1,
  //       TaskStatus.DONE,
  //       mockUser,
  //     );
  //     expect(libraryService.getTaskById).toHaveBeenCalled();
  //     expect(save).toHaveBeenCalled();
  //     expect(result.status).toEqual(TaskStatus.DONE);
  //   });
  // });
});
