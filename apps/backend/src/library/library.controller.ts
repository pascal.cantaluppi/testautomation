import {
  Controller,
  Param,
  Body,
  Get,
  Post,
  Delete,
  Patch,
  UsePipes,
  ValidationPipe,
  ParseIntPipe,
  Logger,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { LibraryService } from './library.service';
import { CreateLibraryDto } from './dto/library.dto';
import { Library } from './library.entity';

@Controller('library')
export class LibraryController {
  private logger = new Logger('LibraryController');
  constructor(private libraryService: LibraryService) {}

  @Get()
  @ApiTags('library')
  @ApiOperation({ description: 'Get all libraries' })
  @HttpCode(HttpStatus.OK)
  getLibraries(): Promise<Library[]> {
    this.logger.verbose(`Retrieving all libraries`);
    return this.libraryService.getLibraries();
  }

  @Get()
  @ApiTags('library')
  @ApiOperation({ description: 'Get library by id' })
  @HttpCode(HttpStatus.OK)
  @Get('/:id')
  getLibraryById(@Param('id', ParseIntPipe) id: number): Promise<Library> {
    return this.libraryService.getLibraryById(id);
  }

  @Post()
  @ApiTags('library')
  @ApiOperation({ description: 'Post new library' })
  @HttpCode(HttpStatus.CREATED)
  @UsePipes(ValidationPipe)
  createLibrary(@Body() createLibraryDto: CreateLibraryDto): Promise<Library> {
    this.logger.verbose(
      `Creating a new library. Data: ${JSON.stringify(createLibraryDto)}`,
    );
    return this.libraryService.createLibrary(createLibraryDto);
  }

  @Patch('/:id/title')
  @ApiTags('library')
  @ApiOperation({ description: 'Patch library title' })
  @HttpCode(HttpStatus.OK)
  updateTaskTitle(
    @Param('id', ParseIntPipe) id: number,
    @Body('title, ') title: string,
  ): Promise<Library> {
    return this.libraryService.updateLibraryTitle(id, title);
  }

  @Delete('/:id')
  @ApiTags('library')
  @ApiOperation({ description: 'Delete a library' })
  @HttpCode(HttpStatus.OK)
  deleteLibraryById(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.libraryService.deleteLibrary(id);
  }
}
