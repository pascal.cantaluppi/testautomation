import React from 'react';
import { Container, Row } from 'react-bootstrap';
import ListItem from './ListItem';

class List extends React.Component {
  constructor(props) {
    super(props);
  }

  renderLibraries = () => {
    var data = Array.from(this.props.libraries);
    return data.map((library, index) => {
      return <ListItem key={index} library={library} />;
    });
  };

  render() {
    return (
      <React.Fragment>
        <Container>
          <Row
            className={['row-cols-1', 'row-cols-md-2', 'row-cols-xl-3'].join(
              ' '
            )}
          >
            {this.renderLibraries()}
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}

export default List;
