import { Nav, Navbar, Container, Image } from 'react-bootstrap';
import Link from 'next/link';

export default function Header() {
  return (
    <header>
      <Container>
        <Navbar variant="light" expand="md">
          <Navbar.Brand className="pt-3">
            <Link href="/">
              <a>
                <Image src="/img/logo.png" alt="Logo" />
              </a>
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto mt-2">
              <Nav.Link>
                <Link href="/" className="nav-link" role="button" passHref>
                  {/* <a className="nav-link" role="button"> */}
                  <b>
                    JavaScript Testing Frameworks
                    <br />
                    &nbsp;&nbsp;&nbsp;for automated Testing
                  </b>
                  {/* </a> */}
                </Link>
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Container>
    </header>
  );
}
