import React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export default function Form() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        Neuer Eintrag
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Library</DialogTitle>
        <DialogContent>
          <DialogContentText>Library Name</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="title"
            //label="Title"
            type="text"
            fullWidth
            variant="standard"
          />
          <DialogContentText>Library Description</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="description"
            //label="Description"
            type="text"
            fullWidth
            variant="standard"
          />
          <DialogContentText>Link to website</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="url"
            //label="Description"
            type="text"
            fullWidth
            variant="standard"
          />
          <DialogContentText>Image location</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="image"
            //label="Description"
            type="text"
            fullWidth
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Abbrechen</Button>
          <Button onClick={handleClose}>Speichern</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
