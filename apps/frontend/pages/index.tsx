import * as React from 'react';
import Head from 'next/head';
import Error from 'next/error';
import Header from '../components/Header';
import List from '../components/List';
import { Jumbotron, Container, Image } from 'react-bootstrap';
import axios from 'axios';

const Home = ({ libraries = {}, statusCode }) => {
  if (statusCode) {
    return <Error statusCode={statusCode} />;
  }

  return (
    <React.Fragment>
      <Head>
        <title>Automated testing - Frontend</title>
        <meta name="description" content="Automated testing - Frontend" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <main>
        <Image src="/img/jumbotron.jpg" fluid alt="jumbo" />
        <Jumbotron className="col-8 mt-5 mx-auto">
          <h1>Automated software testing</h1>
          <p>
            Playground app for JavaScript testing libraries and frameworks used
            for automated software testing.{' '}
          </p>
        </Jumbotron>
        {/* this is where the magic happens */}
        <List libraries={libraries} />
        {/* <Container>
          <Image
            src="/img/components.png"
            alt="components"
            style={{ marginLeft: 100 }}
          />
        </Container> */}
        <div className="parallax paralsec" />
        <Container>
          <div className="mt-3 p-3 mx-auto">
            <h2>What is automated testing?</h2>
            <p>
              Automated testing helps to increase the quality of software by
              validate its functioning before it is moved to production.
            </p>
            <p>
              Predefined scripts for unit testing are executing during the
              deployment flow by the ci/cd pipeline automatically. E2E testing
              scripts then help the qa team to easily test the software, create
              the necessary reports and compare the results with the expected
              target.
            </p>
            <p>
              This procedure results in several benefits such as faster
              delivery, reducing manual testing efforts and higher software
              quality
            </p>
          </div>
        </Container>
      </main>
      <footer className="footer mt-auto mx-auto py-3 bg-light">
        <div className="container text-center">
          <span className="text-muted text-center">
            JacaScript testing guidelines - demo workspace
          </span>
        </div>
      </footer>
    </React.Fragment>
  );
};

Home.getInitialProps = async () => {
  try {
    const res = await axios.get(`https://backend.cantaluppi.org/library`);
    return { libraries: res.data };
  } catch (error) {
    console.error(error);
    return {
      statusCode: error.response ? error.response.status : 500,
    };
  }
};

export default Home;
