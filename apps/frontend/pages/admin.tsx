import * as React from "react";
import Head from "next/head";
import Error from "next/error";
import NavBar from "../components/admin/NavBar";
import Grid from "../components/admin/Grid";
import Form from "../components/admin/Form";
import axios from "axios";

const Admin = ({ libraries = {}, statusCode }) => {
  if (statusCode) {
    return <Error statusCode={statusCode} />;
  }

  return (
    <React.Fragment>
      <Head>
        <title>Automated testing - Dashboard</title>
        <meta name="description" content="Automated testing - Dashboard" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavBar />
      <p />
      <Grid />
      <p />
      <Form />
    </React.Fragment>
  );
};

Admin.getInitialProps = async () => {
  try {
    const res = await axios.get(`https://backend.cantaluppi.org/library`);
    return { libraries: res.data };
  } catch (error) {
    console.error(error);
    return {
      statusCode: error.response ? error.response.status : 500,
    };
  }
};

export default Admin;
